# Wrapper en python para OpenBR [(openbiometrics)](http://openbiometrics.org/)

- Nombre: PyOBR (Python OpenBR wrapper)
- Autor: Iván de Paz Centeno
- Email: ipazce00@estudiantes.unileon.es
- Versión: 1.1
- Fecha: 05/10/2015

1. Descripción
----------------
Wrapper de python para OpenBR 0.5. Proporciona una interfaz sencilla de utilización de algoritmos básicos de OpenBR.
Puesto que OpenBR ha alcanzado la versión 1.1.0 y la versión 0.5 ya no se mantiene oficialmente, se ha modificado para
permitir la detección de rostros utilizando OpenBR 1.1.0. No se ha probado en OpenBR 1.1.0 con el resto de algoritmos
por lo que podría no funcionar en ciertas circunstancias (reconocimiento de rostros).

2. Instrucciones de instalación en Ubuntu/Debian.
--------------------------------------------------

### 1) Instalamos las dependencias para compilar OpenCV:
El compilador GCC, entre otras.
```sh
    $ sudo apt-get update
    $ sudo apt-get install build-essential cmake cmake-curses-gui git unzip  python-pil
```

### 2) Descargamos y compilamos OpenCV 

Podemos descargarlo desde http://sourceforge.net/projects/opencvlibrary/files/

```sh
    $ tar -xf opencv-2.4.X.tar.gz
    $ cd opencv-2.4.X
    $ mkdir build
    $ cd build
    $ cmake -DCMAKE_BUILD_TYPE=Release ..
    $ make -j4
    $ sudo make install
    $ cd ../..
    $ rm -rf opencv-2.4.X*
```

### 3) Instalamos QT5

QT es necesario para compilar OpenBR

```sh
    $ sudo apt-get install qt5-default libqt5svg5-dev qtcreator
```
### 4) Descargamos y compilamos OpenBR
Donde make -jX, X es el número de cores disponibles para repartir el proceso de compilación.
```sh
    $ git clone https://github.com/biometrics/openbr.git
    $ cd openbr
    $ git checkout 1.1.0
    $ git submodule init
    $ git submodule update  
    $ mkdir build
    $ cd build
    $ cmake -DCMAKE_BUILD_TYPE=Release ..
    $ make -j4
    $ sudo make install
```
### 5) Clonamos el repositorio de PyOBR y compilamos la biblioteca de comparación de plantillas:
```sh
    $ git clone https://gitlab.com/ipazce00/PyOBR.git
    $ cd PyOBR/pyobr/cppLib/
    $ make
    $ sudo cp *.so* /usr/lib/
```

3. Ejecución de ejemplos.
---------------------------

Existen dos ejemplos en la carpeta raíz del proyecto: uno para la gestión de
imágenes y otro para la gestión de cámaras.

Mediante la ejecución del siguiente ejemplo se puede verificar si el framework ha sido instalado correctamente:

```sh   
    $ python exampleImage.py
```

La salida esperada es:

```
Set algorithm to FaceDetection+Draw:Dist(L2)
Set enrollAll
Numero de caras detectadas: 1

Coordenadas de la cara: ['243', '61', '430', '430']
Coordenadas de los ojos: ['', '']
Es una cara fiable?: No

Finalizado
```