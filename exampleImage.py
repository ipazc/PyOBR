#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Ejemplo de uso de pyobr wrapper para manejar directamente imágenes
'''

from pyobr.brPyFace import pyOBRFace;
from pyobr.brPyTemplateList import pyOBRTemplateList;
from pyobr.brPyFaceTemplate import pyOBRFaceTemplate;
from pyobr.brPyHandler import openBRHandler;



# Se muestran dos casos posibles: 
#   a) Imagen en fichero. 
#lFaceFile = pyOBRFace(openBRHandler);
#lFaceFile.loadImg("/tmp/frame.jpg");

#   b) Imagen en URL.
lFaceUrl = pyOBRFace(openBRHandler);
lFaceUrl.loadImg("http://referentiel.nouvelobs.com/file/5439234.jpg");
# Ambos casos tienen el mismo tratamiento. 


# Detectamos caras para la imagen URL. El parámetro True fuerza la detección de
# todas las posibles caras de la imagen:
lFacesList = lFaceUrl.detectFace(True);

print "Numero de caras detectadas: " + str(lFacesList.count()) + "\n";

lFaceTemplate = pyOBRFaceTemplate(lFacesList.get(0));

print "Coordenadas de la cara: " + str(lFaceTemplate.getFaceDetectionBounds());
print "Coordenadas de los ojos: " + str(lFaceTemplate.getEyesPosition());

if lFaceTemplate.isAConfidenceDetection():
    lReliableFace = "Si";
else:
    lReliableFace = "No";
    
# Ciertas caras son marcadas como fiables sin serlo, sin embargo permite
# discriminar bastante bien los falsos positivos.
print "Es una cara fiable?: " + lReliableFace;  
print "\nFinalizado\n";