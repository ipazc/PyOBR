#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Python OpenBR Handler interface v0.1
by Iván de Paz Centeno
25/03/2015
'''

import os.path;
from pyobr import lib;
from pyobr import extensionLib;


class pyOBRHandler(object):
  """
  OpenBR Handler class. 
  SINGLETON pattern. Just access the openBRHandler object to get the singleton.
  Initializes/finalizes the handler library.
  """
  __initialized = False;
  __lib = None;
  __extensionLib = None;
  
  """
  ****************************************************
  CONSTRUCTOR
  """
  def __init__(self, pLib, pExtensionLib = None):
    """
    Initializes the face processor.
    """
    self.__lib = pLib;
    self.__extensionLib = pExtensionLib;
    
    if self.__lib:
      self.__lib.br_initialize_default()
      self.__initialized = True;
    
    
  """
  ****************************************************
  __handlerOK() method
  """
  def isInitialized(self):
    """
    Return True if handler is initialized. False if not.
    """
    
    return self.__initialized;
  
  
  
  """
  ****************************************************
  getSDKPath() method
  """
  def getSDKPath(self):
    """
    Returns the path to the OpenBR SDK.
    """
    return self.__lib.br_sdk_path();
  
  
  """
  ****************************************************
  about() method
  """
  def about(self):
    """
    Returns the OpenBR about string.
    """
    return self.__lib.br_about();
  
  
  
  """
  ****************************************************
  __getLib() method
  """
  def getLib(self):
    """
    getter for the lib.
    """
    
    return self.__lib;
  
  
  """
  ****************************************************
  __getLib() method
  """
  def getExtensionLib(self):
    """
    getter for the extension lib.
    """
    
    return self.__extensionLib;
      
  """
  ****************************************************
  __DESTRUCTOR
  """
  def __del__(self):
    """
    Clean up the library access.
    """
    if self.__lib:
      self.__lib.br_finalize();
    
# Singleton defined here:
openBRHandler = pyOBRHandler(lib, extensionLib);

