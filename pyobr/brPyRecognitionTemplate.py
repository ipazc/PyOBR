#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Python OpenBR recognition template interface v0.1
by Iván de Paz Centeno
04/06/2015
'''

import brPyTemplate
import pyobr;

class pyOBRRecognitionTemplate(brPyTemplate.pyOBRTemplate):
  """
  OpenBR recognition template class. 
  Represent an OpenBR recognition template object. 
  
  The OpenBR recognition template object is usually the result of an applied algorithm. Holds all the information
  of the operation, like the metadata, the file path and many other parameters.
  """
  
  """
  ****************************************************
  CONSTRUCTOR
  """
  def __init__(self, pPyOBRTemplate):
    """
    Initializes the template object.
    """
    self._openBRHandler = pPyOBRTemplate._openBRHandler;
    self._openBRTemplate = pPyOBRTemplate._openBRTemplate;
    self._destructionHandled = True;
  
  """
  ****************************************************
  compareTo() method.
  """
  def compareTo(self, pRecognitionTemplate):
    """
    Compares this template with the specified one. Should be a recognition template result.
    """
    return pyobr.extensionLib.compareTemplates(self.getData(), pRecognitionTemplate.getData());