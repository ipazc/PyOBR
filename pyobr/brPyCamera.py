#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Python OpenBR camera v0.2
by Iván de Paz Centeno
30/04/2015
'''

from StringIO import StringIO;
from PIL import Image, ImageDraw;
from brPyFace import pyOBRFace;
from brPyPerson import pyOBRPerson;
from brPyHandler import openBRHandler;
from brPyAgeTemplate import pyOBRAgeTemplate;
from brPyFaceTemplate import pyOBRFaceTemplate;
from brPyGenderTemplate import pyOBRGenderTemplate;
import cv2;
import sys;
import pyobr;
from pyobr import lib;

#By default the openBRHandler is initialized.



class pyOBRCamera():
  """
  OpenBR camera processor class. 
  
  Manages the recording of a camera and the frame evaluation process.
  """
  
  """
  ****************************************************
  CONSTRUCTOR
  """
  def __init__(self, pCameraURI = 0, pOBRHandler = openBRHandler):
    """
    Initializes the camera object.
    """
    self._cameraURI = pCameraURI;
    self._recording = False;
    self._camera = None;
    self._people = [];
    self._openBRHandler = pOBRHandler;
    self.DEFAULT_FRAME_LOCATION = "/tmp/frame.jpg";
    
  """
  """
  def startRecording(self):
    """
    Tries to start the recording of the camera.
    Returns true if could start the recording of the camera, false otherwise.
    """
    if self._recording:
      return False;
    
    self._camera = cv2.VideoCapture(self._cameraURI);
    self._recording = True;
    
    return True;
    
    
    
  """
  """
  def stopRecording(self):
    """
    Stops the recording process of the camera.
    True if could stop the process. False otherwise.
    """
    
    if (not self._recording):
      return False;
    
    self._camera.release();
    self._recording = False;
    
    return True;
    
  """
  """
  def getPeopleList(self):
    """
    Retrieves the current frame people list.
    """
    return self._people;
  
  """
  """
  def clearPeopleList(self):
    """
    Clears the list of people.
    """
    self._people = [];
    
  """
  """
  def processNextFrame(self, pMultipleFaces = False):
    """
    Retrieves a new frame from the camera.
    Returns true if frame generated metadata (detection), false otherwise.
    
    If pMultipleFaces is true, the algorithm will process all the detected faces, else it will process only the first face.
    Be aware that processing multiple faces at the same time may impact in the application performance.
    
    In case of true, this data can be checked with the getPeopleList() method.
    """
    
    if (not self._recording):
        return False;
    
    # we retrieve a new frame from the camera.
    lRetVal, lFrame = self._camera.read();
    lBinaryFrame = cv2.imencode('.jpg', lFrame)[1].tostring();
    lPILImage = Image.open(StringIO(lBinaryFrame));
    
    lPyFaceTemplateList = self.__processFrameForFaces (lBinaryFrame);
    

    lReliableFaces = self.__getReliableFaces(lPyFaceTemplateList);
    
    lNumFaces = len(lReliableFaces);
    if (lNumFaces == 0):
        lPILImage.save(self.DEFAULT_FRAME_LOCATION, "JPEG");
        self.clearPeopleList();
        return False;
      
      
    if (lNumFaces > 1 and not pMultipleFaces):
        lNumFaces = 1;		# we discard the rest of the faces if no multiple faces detection is desired.
	
	
    # Now we retrieve age and gender approximation of the detected face(s) in the frame.
    lProcessedData = self.__processFrame(lBinaryFrame);
    lPyAgeTemplateList = lProcessedData[0];
    lPyGenderTemplateList = lProcessedData[1];
    
    lNewPeopleList = [];
    
    lIteration = 0;
    while (lIteration < lNumFaces):
      # We take the next reliable face index from the faces list.
      lFaceIndex = lReliableFaces[lIteration];
      
      lPerson = self.generatePerson (pyOBRFaceTemplate(lPyFaceTemplateList.get(lFaceIndex)),
				     pyOBRAgeTemplate(lPyAgeTemplateList.get(lFaceIndex)),
				     pyOBRGenderTemplate(lPyGenderTemplateList.get(lFaceIndex)),
				     lPILImage);
      
      # we wrap the person avatar in order to process it and extract its characteristics vector to allow comparisions.
      lFace = pyOBRFace(self._openBRHandler);
      
      lOutput = StringIO();
      lPerson.getAvatar().save(lOutput, format="JPEG");
      lContents = lOutput.getvalue()
      lOutput.close()

      if not lFace.loadImg2(lContents):
          print "\nERROR LOADING AVATAR INTO FACE CLASS.\n";
          
      
      lPyRecognitionTemplateList = lFace.recognition();
      
      #if lPyRecognitionTemplateList.count() == 0:
      #    print "\n[", lIteration, "] ***ERROR: NO FACE RECOGNIZED.*** \n";
      #else: 
      #    print "\n[", lIteration, "] ***face recognized.*** \n";
      lPerson.setRecognitionTemplateList (lPyRecognitionTemplateList);
      
      #lPerson.setIndex(lFaceIndex);
      
      #Probably we need to process this array again after.
      lNewPeopleList.append(lPerson);
      
      #We need to retrieve the previous position in the array of this face. In order to achieve this,
      #we find the nearest face coords in the previous list. It may happen that the nearest face for two persons is the same.
      #If this happens, we must filter both faces to retrieve the nearest one... again.      
      lPerson.setIndex(self.getNearestFacePosition(lPerson));
      
      lIteration = lIteration + 1;
   
    
    # The original people list must be updated.
    self.mergeIntoPeopleList(lNewPeopleList);
    
    lPILImage.save(self.DEFAULT_FRAME_LOCATION, "JPEG");
    
    return True;

  """
  """
  def getNearestFacePosition(self, pPerson):
    """
    Gets the nearest face position index to this person from the list.
    """
    lNearest = 99999;
    lNearestIndex = -1;
    
    lIteration = 0;
    for lPerson in self._people:
        lValue = lPerson.getDistanceToPerson(pPerson);
        if lValue < lNearest:
            lNearest = lValue;
            lNearestIndex = lIteration;
        lIteration = lIteration + 1;
    
    return lNearestIndex;

  """
  """
  def solveDuplicatesInPeopleList(self, pNewPeopleList):
    """
    Solves the duplicates in the list. Two or more persons may point to the same face... we need
    to solve this duplicate by searching the nearest one to that face. The other will be the 
    new fresh face in the scene.
    """
    lUsedIndexes = [];
    lUsedBy = [];
    
    for lPerson in pNewPeopleList:
        lIndex = lPerson.getIndex();
        if (lIndex in lUsedIndexes) and (lIndex > -1):
            ## Warning!! two faces match same index. One of them must be a new face!
            ## We need to find out which one is nearest of its index:
            lUsedIndexIndex = lUsedIndexes.index(lIndex);
            lPersonInConflict = lUsedBy[lUsedIndexIndex];
            lNearestFace = self._people[lIndex];
            
            if (lNearestFace.getDistanceToPerson(lPerson) < lNearestFace.getDistanceToPerson(lPersonInConflict)):
                # lPerson is telling the truth.
                lUsedBy[lUsedIndexIndex] = lPerson;
                # lPersonInConflict is the new face in the scene.
                lPersonInConflict.setIndex(-1);
            else:
                # lPersonInConflict is telling the truth.
                lUsedBy[lUsedIndexIndex] = lPersonInConflict;
                # lPerson is the new face in the scene.
                lPerson.setIndex(-1);
        else:    
            lUsedIndexes.append(lIndex);
            lUsedBy.append(lPerson);

    if (len(pNewPeopleList) > 1):
        for lPerson in pNewPeopleList:
            if lPerson.getIndex() == -1:
                lUsedBy.append(lPerson);
            
    return [lUsedIndexes, lUsedBy];

  """
  """
  def mergeIntoPeopleList(self, pNewPeopleList):
    """
    Merges the new list into the old list.
    """
    # If two or more faces' index points to the same previous face, we need to solve that problem.
    [lUsedIndexes, lUsedBy] = self.solveDuplicatesInPeopleList(pNewPeopleList);
        
    #let's order the face list.
    lTempPeopleList = [];
    lIteration = 0;
    for lPerson in self._people:
        if lIteration in lUsedIndexes:
            #We update here the person data
            lWithPerson = lUsedBy[lUsedIndexes.index(lIteration)];
            self.updatePerson(lPerson, lWithPerson);
            lTempPeopleList.append(lPerson);
            lPerson.setIndex(lIteration);
            
        lIteration = lIteration + 1;
   
    lIteration2 = 0;
    for lPerson in lUsedBy:
        if (lIteration2 >= lIteration):
            lTempPeopleList.append(lPerson);
            lPerson.setIndex(lIteration2);
            
        lIteration2 = lIteration2 + 1;
        
    self._people = lTempPeopleList;
    
  """
  """
  def generatePerson(self, pFaceTemplate, pAgeTemplate, pGenderTemplate, pPILImage):
    """
    Creates a person wrapping information from the templates and returns it.
    """
    lBounds = pFaceTemplate.getFaceDetectionBounds();
    lBox = [int(lBounds[0]), int(lBounds[1]), int(lBounds[0]) + int(lBounds[2]), int(lBounds[1]) + int(lBounds[3])];
    
    draw = ImageDraw.Draw(pPILImage);
    draw.rectangle(lBox, outline=128);
    
    lAvatar = pPILImage.crop(lBox);

    lPerson = pyOBRPerson();
    
    lPerson.setAge(pAgeTemplate.getAge());
    lPerson.setGender(pGenderTemplate.getGender());
    lPerson.setAvatar(lAvatar);
    lPerson.setFaceCoords(lBounds);
    
    return lPerson;
    
  """
  """
  def cropPeopleList(self, pNewCount):
    """
    Crops the people list to fit the specified count.
    """
    while (len (self._people) > pNewCount):
      self._people.pop();
    
    
  """
  """
  def updatePerson(self, pPersonToUpdate, pPerson):
    """
    Updates the person data inside the given person to update.
    
    This is done in order to keep a mean value of each face in the list.
    """
    
    pPersonToUpdate.addAgeMeasure (pPerson.getAge());
    pPersonToUpdate.addGenderMeasure (pPerson.getGender());
    pPersonToUpdate.setAvatar (pPerson.getAvatar());
    pPersonToUpdate.setRecognitionTemplateList (pPerson.getRecognitionTemplateList());
    pPersonToUpdate.setAssociatedFace (pPerson.getAssociatedFace());
  """
  processFrameForFaces function.
  """
  def __processFrameForFaces (self, pFrameImg):
    """
    Fast method in order to retrieve if there's confidence faces inside the image.
    """
    
    gFaceChecker = pyOBRFace(self._openBRHandler);

    if (not gFaceChecker.loadImg2(pFrameImg)):
      return None;
    
    
    return gFaceChecker.detectFace(True);
    
  """
  processFrame() function.
  """
  def __processFrame (self, pFrameImg):
    """
    Processes a new frame in order to retrieve data from the frame like if there's any person inside,
    its age, its gender and its face position.
    
    Returns a list of template lists each being, by order, the face data, the age data and the gender data.
    """
    
    gFaceChecker = pyOBRFace(self._openBRHandler);

    if (not gFaceChecker.loadImg2(pFrameImg)):
      return None;
    
    
    lPyAgeTemplateList = gFaceChecker.detectAge(True);
    lPyGenderTemplateList = gFaceChecker.detectGender(True);
    
    return [lPyAgeTemplateList, lPyGenderTemplateList];

  """
  getReliableFaces() function
  """
  def __getReliableFaces (self, pPyFaceTemplateList):
    """
    Processes a face template list in order to retrieve a list of reliable face indexes.
    """
    lResult = [];
    
    lTemplateCount = pPyFaceTemplateList.count();
    lIteration = 0;
    while (lIteration < lTemplateCount):
      lTemplate = pyOBRFaceTemplate(pPyFaceTemplateList.get(lIteration));
      
      if (lTemplate.isAConfidenceDetection()):
	  lResult.append (lIteration);
	  
      lIteration = lIteration + 1;
      
    
    return lResult;
    

  """
  ****************************************************
  __DESTRUCTOR
  """
  def __del__(self):
    """
    Clean up the class data
    """
    self.stopRecording();
		