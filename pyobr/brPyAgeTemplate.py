#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Python OpenBR age template interface v0.1
by Iván de Paz Centeno
26/03/2015
'''

import brPyTemplate

class pyOBRAgeTemplate(brPyTemplate.pyOBRTemplate):
  """
  OpenBR age template class. 
  Represent an OpenBR age template object. 
  
  The OpenBR template object is usually the result of an applied algorithm. Holds all the information
  of the operation, like the metadata, the file path and many other parameters.
  """
  
  """
  ****************************************************
  CONSTRUCTOR
  """
  def __init__(self, pPyOBRTemplate):
    """
    Initializes the template object.
    """
    self._openBRHandler = pPyOBRTemplate._openBRHandler;
    self._openBRTemplate = pPyOBRTemplate._openBRTemplate;
    self._destructionHandled = True;
    
  """
  ****************************************************
  getAge() method
  """
  def getAge(self):
    """
    Returns the age result of the AgeEstimation algorithm.
    """
    
    return self.getMetadataValue("Age");