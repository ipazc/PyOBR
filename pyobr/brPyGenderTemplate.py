#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Python OpenBR gender template interface v0.1
by Iván de Paz Centeno
10/04/2015
'''

import brPyTemplate

class pyOBRGenderTemplate(brPyTemplate.pyOBRTemplate):
  """
  OpenBR Gender template class. 
  Represent an OpenBR face template object with gender interface operations. 
  
  The OpenBR template object is usually the result of an applied algorithm. Holds all the information
  of the operation, like the metadata, the file path and many other parameters.
  """
  
  """
  ****************************************************
  CONSTRUCTOR
  """
  def __init__(self, pPyOBRTemplate):
    """
    Initializes the template object.
    """
    self._openBRHandler = pPyOBRTemplate._openBRHandler;
    self._openBRTemplate = pPyOBRTemplate._openBRTemplate;
    self._destructionHandled = True;
  
  """
  ****************************************************
  getGender() method
  """
  def getGender(self):
    """
    Returns the gender of the recognized face. 0 for male, 1 for female.
    """
    lGender = self.getMetadataValue("Gender");
    
    if lGender == "Male":
      lResult = 0;
    else:
      lResult = 1;
      
    return lResult;