#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Python OpenBR Face interface v0.2
by Iván de Paz Centeno
26/03/2015
'''

import pyobr
import os.path
import brPyHandler
from brPyTemplate import pyOBRTemplate;
from brPyTemplateList import pyOBRTemplateList;
import urllib2
from ctypes import *
from PIL import Image

class pyOBRFace(object):
  """
  OpenBR face class. 
  Allows to process biometrics on a face.
  """
  __imgTemplate = None;
  __imgObject = None;
  __openBRHandler = None;
  
  """
  ****************************************************
  CONSTRUCTOR
  """
  def __init__(self, pOpenBRHandler, pTemplateToWrap = None):
    """
    Initializes the face processor.
    """
    self.__imgTemplate = None;
    self.__imgObject = None;
    self.__openBRHandler = pOpenBRHandler;
    
    if (pTemplateToWrap):
      self.__imgTemplate = pTemplateToWrap;
    
  """
  ****************************************************
  __handlerOK() method
  """
  def __handlerOK(self):
    """
    Checks if the handler is OK to perform the OpenBR operations.
    
    Returns True if handler set and initialized. False if not.
    """
    
    return self.__openBRHandler and self.__openBRHandler.isInitialized();
  
 
  """
  ****************************************************
  loadImg() method
  """
  def loadImg(self, pImagePath):
    """
    Loads an image into the openbr lib. 
    It may be an image URI or an image URL.
 
    Returns True if could load the specified image. False if not.
    """
    
    if not self.__handlerOK():
      return False;
    
    lImage = None;
    lResult = False;
    
    # First we test if it's a local URI.
    if os.path.exists(pImagePath):
      with open(pImagePath, 'rb') as f:
        lImage = f.read();

    # or a URL
    elif pImagePath.lower().startswith("http"):
      try:
        lResponse=urllib2.urlopen(pImagePath);
      except:
        print pImagePath + " is not a valid URI or valid URL";
      else:
        lImage = lResponse.read();
    else:
      print "Invalid image referenced. A local URI or an URL must be referenced in order for loadImg to work.";
    
    
    # lImage should have some content now to continue the process.
    if lImage:
      lResult = self.__loadImg(lImage);
    
    #print "Loading image.";
    return lResult;
  
  """
  loadImg2() method
  """
  def loadImg2(self, pDataToLoad):
    """
    Loads a given image array of bytes to the openbr library.
    """
    return self.__loadImg(pDataToLoad);
  
  """
  ****************************************************
  __loadImg() method
  """  
  def __loadImg(self, pDataToLoad):
    """
    loads the image data (bytes) into the OpenBR library.
    
    Returns True if could load the image. False if not.
    """
    lResult = False;
    
    if pDataToLoad:
      self.__imgTemplate = pyOBRTemplate(self.__openBRHandler, pyobr.lib.br_load_img(pDataToLoad, len(pDataToLoad)));
      lResult = True;
      
    return lResult;
    
    
  """
  ****************************************************
  saveImg() method
  """  
  def saveImg(self, pDestination):
    """
    Saves the face image into the specified pDestination.
    
    Returns True if could save the image. False if not.
    """
    
    lSaved = False;
    
    # We need to unload the img from the library to be able to store it.
    if not self.__unloadImg():
      return lSaved;
    
    # Now we can save it inside the location
    if self.__imgObject and pDestination:
      # Warning: the image uses some kind of BGR format. We need to swap bytes (and do something more) to make it RGB.
      self.__imgObject.save(pDestination);
      lSaved = True;

  
    return lSaved;
  
  
  
  """
  ****************************************************
  getImg() method
  """
  def getImg(self):
    """
    Retrieves the image bytes from the OpenBR library. None otherwise.
    """
    
    # We need to unload the img from the library to be able to return it.
    if not self.__unloadImg():
      return None;
    
    
    return self.__imgObject;
    
  """
  ****************************************************
  recognition() method
  """
  def recognition(self):
    """
    Applies a face recognition algorithm to the face in order to extract
    the characteristics of the face.
    """
    if (not self.__handlerOK()):
      return None;

    pyobr.lib.br_set_property('algorithm', 'FaceRecognition');

    lTemplateList = pyobr.lib.br_enroll_template(self.__imgTemplate.getData());
    
    return pyOBRTemplateList(self.__openBRHandler, lTemplateList);
    
  """
  ****************************************************
  detectFace() method
  """
  def detectFace(self, pAllFaces = False):
    """
    Detects the face/s inside the image (with its bounds and eyes position).
    
    If pÀllFaces is set, the method will find all the faces that is capable of in the current image.
    Returns the pyOBRTemplateList as a result of the algorithm application; None otherwise.
    """

    if (not self.__handlerOK()):
      return None;

    pyobr.lib.br_set_property('algorithm', 'FaceDetection+Draw:Dist(L2)');

    if pAllFaces:
      pyobr.lib.br_set_property('enrollAll', '');

    lTemplateList = pyobr.lib.br_enroll_template(self.__imgTemplate.getData());

    return pyOBRTemplateList(self.__openBRHandler, lTemplateList);

  """
  ****************************************************
  compareTo() method
  """
  def compareTo(self, pWithFace):
    """
    Compares this face to other face and returns a similarity float value.
    
    Note: Both faces MUST be previously recognized with the detectFace() method (once per face).
    """
    if (not self.__handlerOK()):
      return None;
  
    return pyobr.extensionLib.compareTemplates(self.__imgTemplate.getData(), pWithFace.__imgTemplate.getData());
    


  """
  ****************************************************
  detectGender() method
  """
  def detectGender(self, pAllFaces = False):
    """
    Detects the gender of the given face.
    """
    if (not self.__handlerOK()):
      return None;
    
    pyobr.lib.br_set_property('algorithm', 'GenderEstimation')

    if pAllFaces:
      pyobr.lib.br_set_property('enrollAll', '');

    lTemplateList = pyobr.lib.br_enroll_template(self.__imgTemplate.getData());
    
    return pyOBRTemplateList(self.__openBRHandler, lTemplateList);
    
    
  """
  ****************************************************
  detectAge() method
  """
  def detectAge(self, pAllFaces = False):
    """
    Detects the age of the given face.
    
    Returns the PyOBRTemplateList as a result of the algorithm application; None othwerwise.
    """
    if (not self.__handlerOK()):
      return None;
    
    pyobr.lib.br_set_property('algorithm', 'AgeEstimation')

    if pAllFaces:
      pyobr.lib.br_set_property('enrollAll', '');

    lTemplateList = pyobr.lib.br_enroll_template(self.__imgTemplate.getData());
    
    return pyOBRTemplateList(self.__openBRHandler, lTemplateList);
      
    
  """
  ****************************************************
  __unloadImg() method
  """
  def __unloadImg(self):
    """
    Unloads the actual image from the OpenBR library and returns it as an array of bytes.
    This doesn't clean up its resources in the OpenBR lib. That means we can still work
    with it inside the library.
    
    Returns True if could unload the image from the library. False if not.
    """
    
    if (not self.__handlerOK()):
      return False;
    
    lResult = False;
    
    if self.__imgTemplate.getData():
    #
      lRows = pyobr.lib.br_img_rows(self.__imgTemplate.getData());
      lCols = pyobr.lib.br_img_cols(self.__imgTemplate.getData());
      lChannels = pyobr.lib.br_img_channels(self.__imgTemplate.getData());
      
      lImg = pyobr.lib.br_unload_img(self.__imgTemplate.getData());
      if lChannels == 0:
        lChannels = 1;
      
      lSize = lRows * lCols * lChannels;
      
      # Finally we save the image from the string chunk into an array of bytes that we can write to disk.
      self.__imgObject = Image.fromstring('RGB', (lCols, lRows), string_at(addressof(lImg.contents), lSize));
      lResult = True;
    #
    
    return lResult;
  
  
  
  """
  ****************************************************
  __DESTRUCTOR
  """
  def __del__(self):
    """
    Clean up the class data
    """
    # not need to unload the image. The unload is done to retrieve the actual 
    # image from the library, no to clean up its resources. That means 
    # the image can be unloaded as times as we want.
    
