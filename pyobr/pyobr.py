#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Python OpenBR API v0.1
by Iván de Paz Centeno
25/03/2015
'''

from ctypes import *

OPENBR_DEFAULT_DIR = '/usr/local/lib/libopenbr.so';
EXTENSION_DEFAULT_DIR = '/usr/lib/libCompareTemplates.so';

lib = cdll.LoadLibrary(OPENBR_DEFAULT_DIR);
extensionLib = cdll.LoadLibrary(EXTENSION_DEFAULT_DIR);

'''
************************************
 const char * br_about ()

 Description: 
    Wraps br::Context::about()
************************************
'''
lib.br_about.restype = c_char_p;


'''
************************************
 void br_cat (int num_input_galleries, const char *input_galleries[], const char *output_gallery)

 Description: 
    Wraps br::Cat()
************************************
'''
lib.br_cat.argtypes = [c_int, c_char_p, c_char_p];


'''
************************************
 void br_deduplicate (const char *input_gallery, const char *output_gallery, const char *threshold)

 Description: 
    Removes duplicate templates in a gallery.
************************************
'''
lib.br_deduplicate.argtypes = [c_char_p, c_char_p, c_char_p];


'''
************************************
 void br_cluster (int num_simmats, const char *simmats[], float aggressiveness, const char *csv)

 Description: 
    Clusters one or more similarity matrices into a list of subjects.
************************************
'''
lib.br_cluster.argtypes = [c_int, c_char_p, c_float, c_char_p];


'''
************************************
 void br_combine_masks (int num_input_masks, const char *input_masks[], const char *output_mask, const char *method)

 Description: 
    Combines several equal-sized mask matrices.
************************************
'''
lib.br_combine_masks.argtypes = [c_int, c_char_p, c_char_p, c_char_p];


'''
************************************
 void br_compare (const char *target_gallery, const char *query_gallery, const char *output="")

 Description: 
    Compares each template in the query gallery to each template in the target gallery.
************************************
'''
lib.br_compare.argtypes = [c_char_p, c_char_p, c_char_p];


'''
************************************
 void br_compare_n (int num_targets, const char *target_galleries[], const char *query_gallery, const char *output)

 Description: 
    Convenience function for comparing to multiple targets.
************************************
'''
lib.br_compare_n.argtypes = [c_int, c_char_p, c_char_p, c_char_p];


'''
************************************
 void br_pairwise_compare (const char *target_gallery, const char *query_gallery, const char *output="")

 Description: 
    Wraps br::Convert()
************************************
'''
lib.br_pairwise_compare.argtypes = [c_char_p, c_char_p, c_char_p];


'''
************************************
 void br_convert (const char *file_type, const char *input_file, const char *output_file)

 Description: 
    No description.
************************************
'''
lib.br_convert.argtypes = [c_char_p, c_char_p, c_char_p];


'''
************************************
 void br_enroll (const char *input, const char *gallery="")

 Description: 
    Constructs template(s) from an input.
************************************
'''
lib.br_enroll.argtypes = [c_char_p, c_char_p];


'''
************************************
 void br_enroll_n (int num_inputs, const char *inputs[], const char *gallery="")

 Description: 
    Convenience function for enrolling multiple inputs.
************************************
'''
lib.br_enroll_n.argtypes = [c_int, c_char_p, c_char_p];


'''
************************************
 void br_project (const char *input, const char *output)

 Description: 
    A naive alternative to br_enroll.
************************************
'''
lib.br_project.argtypes = [c_char_p, c_char_p];


'''
************************************
 float br_eval (const char *simmat, const char *mask, const char *csv="", int matches=0)

 Description: 
    Creates a .csv file containing performance metrics from evaluating the similarity matrix using the mask matrix.
************************************
'''
lib.br_eval.restype = c_float;
lib.br_eval.argtypes = [c_char_p, c_char_p, c_char_p, c_int];


'''
************************************
 float br_inplace_eval (const char *simmat, const char *target, const char *query, const char *csv="")

 Description: 
    Creates a .csv file containing performance metrics from evaluating the similarity matrix using galleries containing ground truth labels.
************************************
'''
lib.br_inplace_eval.restype = c_float;
lib.br_inplace_eval.argtypes = [c_char_p, c_char_p, c_char_p, c_char_p];


'''
************************************
 void br_eval_classification (const char *predicted_gallery, const char *truth_gallery, const char *predicted_property="", const char *truth_property="")

 Description: 
    Evaluates and prints classification accuracy to terminal.
************************************
'''
lib.br_eval_classification.argtypes = [c_char_p, c_char_p, c_char_p, c_char_p];


'''
************************************
 void br_eval_clustering (const char *csv, const char *gallery, const char *truth_property)

 Description: 
    Evaluates and prints clustering accuracy to the terminal.
************************************
'''
lib.br_eval_clustering.argtypes = [c_char_p, c_char_p, c_char_p];


'''
************************************
 float br_eval_detection (const char *predicted_gallery, const char *truth_gallery, const char *csv="", bool normalize=false, int minSize=0)

 Description: 
    Evaluates and prints detection accuracy to terminal.
************************************
'''
lib.br_eval_detection.restype = c_float;
lib.br_eval_detection.argtypes = [c_char_p, c_char_p, c_char_p, c_int, c_int];


'''
************************************
 float br_eval_landmarking (const char *predicted_gallery, const char *truth_gallery, const char *csv="", int normalization_index_a=0, int normalization_index_b=1)

 Description: 
    Evaluates and prints landmarking accuracy to terminal.
************************************
'''
lib.br_eval_landmarking.restype = c_float;
lib.br_eval_landmarking.argtypes = [c_char_p, c_char_p, c_char_p, c_int, c_int];


'''
************************************
 void br_eval_regression (const char *predicted_gallery, const char *truth_gallery, const char *predicted_property="", const char *truth_property="")

 Description: 
    Evaluates regression accuracy to disk.
************************************
'''
lib.br_eval_regression.argtypes = [c_char_p, c_char_p, c_char_p, c_char_p];


'''
************************************
 void br_finalize ()

 Description: 
    Wraps br::Context::finalize()
************************************
'''
#lib.br_finalize.restype = c_int;

'''
************************************
 void br_fuse (int num_input_simmats, const char *input_simmats[], const char *normalization, const char *fusion, const char *output_simmat)

 Description: 
    Perform score level fusion on similarity matrices.
************************************
'''
lib.br_fuse.argtypes = [c_int, c_char_p, c_char_p, c_char_p, c_char_p];


'''
************************************
 void br_initialize (int &argc, char *argv[], const char *sdk_path="", bool use_gui=false)

 Description: 
    Wraps br::Context::initialize()
************************************
'''
lib.br_initialize.argtypes = [c_int, c_char_p, c_char_p, c_int];


'''
************************************
 void br_initialize_default ()

 Description: 
    Wraps br::Context::initialize() with default arguments.
************************************
'''


'''
************************************
 bool br_is_classifier (const char *algorithm)

 Description: 
    Wraps br::IsClassifier()
************************************
'''
lib.br_is_classifier.restype = c_int;
lib.br_is_classifier.argtypes = [c_char_p];


'''
************************************
 void br_make_mask (const char *target_input, const char *query_input, const char *mask)

 Description: 
    Constructs a Mask Matrix from target and query inputs.
************************************
'''
lib.br_make_mask.argtypes = [c_char_p, c_char_p, c_char_p];


'''
************************************
 void br_make_pairwise_mask (const char *target_input, const char *query_input, const char *mask)

 Description: 
    Constructs a Mask Matrix from target and query inputs considering the target and input sets to be definint pairwise comparisons.
************************************
'''
lib.br_make_pairwise_mask.argtypes = [c_char_p, c_char_p, c_char_p];


'''
************************************
 int br_most_recent_message (char *buffer, int buffer_length)

 Description: 
    Returns the most recent line sent to stderr.
************************************
'''
lib.br_most_recent_message.restype = c_int;
lib.br_most_recent_message.argtypes = [c_char_p, c_int];


'''
************************************
 int br_objects (char *buffer, int buffer_length, const char *abstractions=".*", const char *implementations=".*", bool parameters=true)

 Description: 
    Returns names and parameters for the requested objects.
************************************
'''
lib.br_objects.restype = c_int;
lib.br_objects.argtypes = [c_char_p, c_int, c_char_p, c_char_p, c_int];


'''
************************************
 bool br_plot (int num_files, const char *files[], const char *destination, bool show=false)

 Description: 
    Renders recognition performance figures for a set of .csv files created by br_eval.
************************************
'''
lib.br_plot.restype = c_int;
lib.br_plot.argtypes = [c_int, c_char_p, c_char_p, c_int];


'''
************************************
 bool br_plot_detection (int num_files, const char *files[], const char *destination, bool show=false)

 Description: 
    Renders detection performance figures for a set of .csv files created by br_eval_detection.
************************************
'''
lib.br_plot_detection.restype = c_int;
lib.br_plot_detection.argtypes = [c_int, c_char_p, c_char_p, c_int];


'''
************************************
 bool br_plot_landmarking (int num_files, const char *files[], const char *destination, bool show=false)

 Description: 
    Renders landmarking performance figures for a set of .csv files created by br_eval_landmarking.
************************************
'''
lib.br_plot_landmarking.restype = c_int;
lib.br_plot_landmarking.argtypes = [c_int, c_char_p, c_char_p, c_int];


'''
************************************
 bool br_plot_metadata (int num_files, const char *files[], const char *columns, bool show=false)

 Description: 
    Renders metadata figures for a set of .csv files with specified columns.
************************************
'''
lib.br_plot_metadata.restype = c_int;
lib.br_plot_metadata.argtypes = [c_int, c_char_p, c_char_p, c_int];


'''
************************************
 float br_progress ()

 Description: 
    Wraps br::Context::progress()
************************************
'''
lib.br_progress.restype = c_float;


'''
************************************
 void br_read_pipe (const char *pipe, int *argc, char ***argv)

 Description: 
    Read and parse arguments from a named pipe.
************************************
'''
lib.br_read_pipe.argtypes = [c_char_p, c_void_p, c_char_p];


'''
************************************
 int br_scratch_path (char *buffer, int buffer_length)

 Description: 
    Wraps br::Context::scratchPath()
************************************
'''
lib.br_scratch_path.restype = c_int;
lib.br_scratch_path.argtypes = [c_char_p, c_int];


'''
************************************
 const char * br_sdk_path ()

 Description: 
    Returns the full path to the root of the SDK.
************************************
'''
lib.br_sdk_path.restype = c_char_p;


'''
************************************
 void br_get_header (const char *matrix, const char **target_gallery, const char **query_gallery)

 Description: 
    Retrieve the target and query inputs in the BEE matrix header.
************************************
'''
lib.br_get_header.argtypes = [c_char_p, c_char_p, c_char_p];


'''
************************************
 void br_set_header (const char *matrix, const char *target_gallery, const char *query_gallery)

 Description: 
    Update the target and query inputs in the BEE matrix header.
************************************
'''
lib.br_set_header.argtypes = [c_char_p, c_char_p, c_char_p];


'''
************************************
 void br_set_property (const char *key, const char *value)

 Description: 
    Wraps br::Context::setProperty()
************************************
'''
lib.br_set_property.argtypes = [c_char_p, c_char_p];


'''
************************************
 int br_time_remaining ()

 Description: 
    Wraps br::Context::timeRemaining()
************************************
'''
lib.br_time_remaining.restype = c_int;


'''
************************************
 void br_train (const char *input, const char *model="")

 Description: 
    Trains the br::Transform and br::Comparer on the input.
************************************
'''
lib.br_train.argtypes = [c_char_p, c_char_p];


'''
************************************
 void br_train_n (int num_inputs, const char *inputs[], const char *model="")

 Description: 
    Convenience function for training on multiple inputs.
************************************
'''
lib.br_train_n.argtypes = [c_int, c_char_p, c_char_p];


'''
************************************
 const char * br_version ()

 Description: 
    Wraps br::Context::version()
************************************
'''
lib.br_version.restype = c_char_p;


'''
************************************
 void br_slave_process (const char *baseKey)

 Description: 
    For internal use via ProcessWrapperTransform.
************************************
'''
lib.br_slave_process.argtypes = [c_char_p];


'''
************************************
 br_template br_load_img (const char *data, int len)

 Description: 
    Load an image from a string buffer. Easy way to pass an image in memory from another programming language to openbr.
************************************
'''
lib.br_load_img.restype = c_void_p;
lib.br_load_img.argtypes = [c_char_p, c_int];


'''
************************************
 unsigned char * br_unload_img (br_template tmpl)

 Description: 
    Unload an image to a string buffer. Easy way to pass an image from openbr to another programming language.
************************************
'''
lib.br_unload_img.restype = POINTER(c_ubyte);
lib.br_unload_img.argtypes = [c_void_p];


'''
************************************
 br_template_list br_template_list_from_buffer (const char *buf, int len)

 Description: 
    Deserialize a br::TemplateList from a buffer. Can be the buffer for a .gal file, since they are just a TemplateList serialized to disk.
************************************
'''
lib.br_template_list_from_buffer.restype = c_void_p;
lib.br_template_list_from_buffer.argtypes = [c_char_p, c_int];


'''
************************************
 void br_free_template (br_template tmpl)

 Description: 
    Free a br::Template's memory.
************************************
'''
lib.br_free_template.argtypes = [c_void_p];


'''
************************************
 void br_free_template_list (br_template_list tl)

 Description: 
    Free a br::TemplateList's memory.
************************************
'''
lib.br_free_template_list.argtypes = [c_void_p];


'''
************************************
 void br_free_output (br_matrix_output output)

 Description: 
    Free a br::Output's memory.
************************************
'''
lib.br_free_output.argtypes = [c_void_p];


'''
************************************
 int br_img_rows (br_template tmpl)

 Description: 
    Get the number of rows in an image.
************************************
'''
lib.br_img_rows.restype = c_int;
lib.br_img_rows.argtypes = [c_void_p];


'''
************************************
 int br_img_cols (br_template tmpl)

 Description: 
    Get the number of columns in an image.
************************************
'''
lib.br_img_cols.restype = c_int;
lib.br_img_cols.argtypes = [c_void_p];


'''
************************************
 int br_img_channels (br_template tmpl)

 Description: 
    Get the number of channels in an image.
************************************
'''
lib.br_img_channels.restype = c_int;
lib.br_img_channels.argtypes = [c_void_p];


'''
************************************
 bool br_img_is_empty (br_template tmpl)

 Description: 
    Returns if the image is empty.
************************************
'''
lib.br_img_is_empty.restype = c_int;
lib.br_img_is_empty.argtypes = [c_void_p];


'''
************************************
 int br_get_filename (char *buffer, int buffer_length, br_template tmpl)

 Description: 
    Get the filename for a br::Template.
************************************
'''
lib.br_get_filename.restype = c_int;
lib.br_get_filename.argtypes = [c_char_p, c_int, c_void_p];


'''
************************************
 void br_set_filename (br_template tmpl, const char *filename)

 Description: 
    Set the filename for a br::Template.
************************************
'''
lib.br_set_filename.argtypes = [c_void_p, c_char_p];


'''
************************************
 int br_get_metadata_string (char *buffer, int buffer_length, br_template tmpl, const char *key)

 Description: 
    Get metadata as a string for the given key in the given template.
************************************
'''
lib.br_get_metadata_string.restype = c_int;
lib.br_get_metadata_string.argtypes = [c_char_p, c_int, c_void_p, c_char_p];


'''
************************************
 br_template_list br_enroll_template (br_template tmpl)

 Description: 
    Enroll a br::Template from the C API! Returns a pointer to a br::TemplateList.
************************************
'''
lib.br_enroll_template.restype = c_void_p;
lib.br_enroll_template.argtypes = [c_void_p];


'''
************************************
 void br_enroll_template_list (br_template_list tl)

 Description: 
    Enroll a br::TemplateList from the C API!
************************************
'''
lib.br_enroll_template_list.argtypes = [c_void_p];


'''
************************************
 br_matrix_output br_compare_template_lists (br_template_list target, br_template_list query)

 Description: 
    Compare br::TemplateLists from the C API!
************************************
'''
lib.br_compare_template_lists.restype = c_void_p;
lib.br_compare_template_lists.argtypes = [c_void_p, c_void_p];


'''
************************************
 float br_get_matrix_output_at (br_matrix_output output, int row, int col)

 Description: 
    Get a value in the br::MatrixOutput.
************************************
'''
lib.br_get_matrix_output_at.restype = c_float;
lib.br_get_matrix_output_at.argtypes = [c_void_p, c_int, c_int];


'''
************************************
 br_template br_get_template (br_template_list tl, int index)

 Description: 
    Get a pointer to a br::Template at a specified index.
************************************
'''
lib.br_get_template.restype = c_void_p;
lib.br_get_template.argtypes = [c_void_p, c_int];


'''
************************************
 int br_num_templates (br_template_list tl)

 Description: 
    Get the number of br::Templates in a br::TemplateList.
************************************
'''
lib.br_num_templates.restype = c_int;
lib.br_num_templates.argtypes = [c_void_p];


'''
************************************
 br_gallery br_make_gallery (const char *gallery)

 Description: 
    Initialize a br::Gallery.
************************************
'''
lib.br_make_gallery.restype = c_void_p;
lib.br_make_gallery.argtypes = [c_char_p];


'''
************************************
 br_template_list br_load_from_gallery (br_gallery gallery)

 Description: 
    Read br::TemplateList from br::Gallery.
************************************
'''
lib.br_load_from_gallery.restype = c_void_p;
lib.br_load_from_gallery.argtypes = [c_void_p];


'''
************************************
 void br_add_template_to_gallery (br_gallery gallery, br_template tmpl)

 Description: 
    Write a br::Template to the br::Gallery on disk.
************************************
'''
lib.br_add_template_to_gallery.argtypes = [c_void_p, c_void_p];


'''
************************************
 void br_add_template_list_to_gallery (br_gallery gallery, br_template_list tl)

 Description: 
    Write a br::TemplateList to the br::Gallery on disk.
************************************
'''
lib.br_add_template_list_to_gallery.argtypes = [c_void_p, c_void_p];


'''
************************************
 void br_close_gallery (br_gallery gallery)

 Description: 
    Close the br::Gallery.
************************************
'''
lib.br_close_gallery.argtypes = [c_void_p];


'''
************************************
 float compareTemplates(void* pTemplateA, void* pTemplateB);
    
 Description:
    Compares two templates and returns its similarity value.
************************************
'''
extensionLib.compareTemplates.argtypes = [c_void_p, c_void_p];
extensionLib.compareTemplates.restype = c_float;


'''
************************************
 int templateSize(void* pTemplate);
    
 Description:
    Returns the size in bytes of the given template.
************************************
'''
extensionLib.templateSize.argtypes = [c_void_p];
extensionLib.templateSize.restype = c_int;

'''
************************************
 int templateMatrixSize(void* pTemplate);
    
 Description:
    Returns the size in bytes of the given template matrix.
************************************
'''
extensionLib.templateMatrixSize.argtypes = [c_void_p];
extensionLib.templateMatrixSize.restype = c_int;


'''
************************************
 int templateMatrixElementsCount(void* pTemplate);
    
 Description:
    Returns the number of elements hold by the template matrix.
************************************
'''
extensionLib.templateMatrixElementsCount.argtypes = [c_void_p];
extensionLib.templateMatrixElementsCount.restype = c_int;