#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Python OpenBR person bean v0.1
by Iván de Paz Centeno
28/04/2015
'''
import numpy;

def getIntegerMean(pMeanObj):
  """
  Computes the integer mean of the given mean object.
  """
  if (pMeanObj[1] == 0):
      pMeanObj[1] = 1;

  return round(pMeanObj[0] / pMeanObj[1]);
     
     
class pyOBRPerson():
  """
  OpenBR person class. 
  
  Represents a person from a scene.
  """
  
  """
  ****************************************************
  CONSTRUCTOR
  """
  def __init__(self):
    """
    Initializes the person object.
    """
    self._asociatedFace = None;
    self._ageMean = [0,0];
    self._ageMean2 = [0,0];             #Used to calibrate the first mean if needed.
    self._genderMean = [0,0];
    self._genderMean2 = [0,0];          #Used to calibrate the first mean if needed.
    self._faceCoords = None;
    self._avatar = None;
    self._index = 0;
    self._fullname = None;
    self._realAge = 0;
    self._realGender = 0;
    self._relatedFaceHistoryIndex = -1;
    self._sinceDate = None;
    self._isAHistoryFace = False;
    self._unrecognizedCount = 0;        #Number of times this person wasn't recognized.
    self._recognitionTemplateList = 0;
    
    self.DEFAULT_MEAN_RESET_COUNT = 10; #the second mean is reseted each 40 seconds
    self.MEAN_LIMIT_THRESHOLD = 4;      #Threshold of +-4 years. If the difference between mean2 and mean1 is greater 
                                        #than this value, mean1 is recalibrated.
    self.UNRECOGNIZED_THRESHOLD = 30;    #Number of times a person must be unrecognized to store it in the history as 
                                        #a new face.
       
  
  """
  """
  def setRecognitionTemplateList(self, pRecognitionTemplate):
    """
    Associates this face with a person.
    """
    
    self._recognitionTemplateList = pRecognitionTemplate;

    
  """
  """
  def getRecognitionTemplateList(self):
    """
    Returns this person associated face.
    """
  
    return self._recognitionTemplateList;


  """
  """
  def setAssociatedFace(self, pFace):
    """
    Associates this face with a person.
    """
    
    self._asociatedFace = pFace;

    
  """
  """
  def getAssociatedFace(self):
    """
    Returns this person associated face.
    """
  
    return self._asociatedFace;


  """
  """
  def setFaceCoords(self, pFaceRect):
    """
    Sets the coords of the face in a numpy format.
    """
    lFaceRect = [ int(pFaceRect[0]), int(pFaceRect[1]), int(pFaceRect[2]), int(pFaceRect[3]) ];


    self._faceCoords = numpy.array([ 
        [lFaceRect[0], lFaceRect[1]],
        [lFaceRect[0] + lFaceRect[2], lFaceRect[1]],
        [lFaceRect[0], lFaceRect[1] + lFaceRect[3]],
        [lFaceRect[0] + lFaceRect[2], lFaceRect[1] + lFaceRect[3]]
    ]);

  """
  """
  def getFaceCoords(self):
    """
    Returns the face coords in a numpy format.
    """
    return self._faceCoords;

  """
  """
  def __vectorsDistance(self, pVec1, pVec2):
    """
    Returns the distance between two vectors.
    """
    return numpy.linalg.norm(pVec1-pVec2)
    
    
  """
  """
  def getDistanceToPerson(self, pPerson):
    """
    Returns the distance from this person to the specified person.
    """
    lLesserValue = 999999;

    lBox1 = self.getFaceCoords();
    lBox2 = pPerson.getFaceCoords();
    
    #We find the lesser segment value between both squares.
    for lVector1 in lBox1:
        for lVector2 in lBox2:
            lValue = self.__vectorsDistance(lVector1, lVector2);
            if (lValue < lLesserValue):
                lLesserValue = lValue;

    return lLesserValue;
  
  
  """
  """
  def setIndex(self, pNewIndex):
    """
    Sets the index of the current person.
    """
    self._index = pNewIndex;
    
  """
  """
  def getIndex(self):
    """
    Retrieves the index of the current person.
    """
    return self._index;
    
  """
  """
  def setAge(self, pAge):
    """
    Sets the age of this person
    """
    self._ageMean[0] = float(pAge);
    self._ageMean[1] = 1;
  
  """
  """
  def getAge(self):
    """
    Returns the age of this person
    """
    return int(getIntegerMean(self._ageMean));
  
  
  def __getAge2(self):
    """
    Returns the age of this person
    """
    return int(getIntegerMean(self._ageMean2));
 
  """
  """
  def getAgeMeanComponents(self):
    """
    Returns the components of the age used to calculate the mean value.
    """
    return self._ageMean;

  """
  """
  def getGenderMeanComponents(self):
    """
    Returns the components of the gender used to calculate the mean value.
    """
    return self._genderMean;

  """
  """
  def setGender(self, pGender):
    """
    Sets the gender of this person.
    """
    self._genderMean[0] = float(pGender);
    self._genderMean[1] = 1;
    
    
  """
  """
  def getGender(self):
    """
    Returns the gender of this person.
    """
    return int(round(getIntegerMean(self._genderMean)));
  
  
  """
  """
  def __getGender2(self):
    """
    Returns the gender of this person.
    """
    return int(round(getIntegerMean(self._genderMean2)));
  
  
  
  """
  """
  def addAgeMeasure (self, pAge):
    """
    Updates the mean value of the age with the given new age.
    """

    if (self._ageMean2[1] % self.DEFAULT_MEAN_RESET_COUNT == 0):
        if (abs(self.getAge() - self.__getAge2()) > self.MEAN_LIMIT_THRESHOLD):
            self._ageMean[0] = self._ageMean2[0];
            self._ageMean[1] = self._ageMean2[1];
        self._ageMean2 = [0, 0];
    
    self._ageMean[0] = self._ageMean[0] + float(pAge);
    self._ageMean[1] = self._ageMean[1] + 1;
        
    self._ageMean2[0] = self._ageMean2[0] + float(pAge);
    self._ageMean2[1] = self._ageMean2[1] + 1;
    
    
  """
  """
  def addGenderMeasure (self, pGender):
    """
    Updates the mean value of the gender with the given new gender.
    """
    if (self._genderMean2[1] % self.DEFAULT_MEAN_RESET_COUNT == 0):
        if (abs(self.getGender() - self.__getGender2()) > self.MEAN_LIMIT_THRESHOLD):
            self._genderMean[0] = self._genderMean2[0];
            self._genderMean[1] = self._genderMean2[1];
        self._genderMean2 = [0, 0];
        
    self._genderMean[0] = self._genderMean[0] + float(pGender);
    self._genderMean[1] = self._genderMean[1] + 1;
    
    self._genderMean2[0] = self._genderMean2[0] + float(pGender);
    self._genderMean2[1] = self._genderMean2[1] + 1;
    
  """
  """
  def setAvatar(self, pAvatar):
    """
    Sets the new avatar of the person.
    """
    self._avatar = pAvatar;
    
  """
  """
  def getAvatar(self):
    """
    Gets the avatar of the person.
    """
    return self._avatar;
    
 
  """
  """
  def getFullName(self):
    """
    Returns the full name of this person.
    """
    return self._fullname;


  """
  """
  def setFullName(self, pFullname):
    """
    Sets the full name for this person.
    """
    self._fullname = pFullname;
    
  """
  """
  def getRealAge(self):
    """
    Returns the real age of this person.
    """
    return self._realAge;


  """
  """
  def setRealAge(self, pRealAge):
    """
    Sets the real age of this person.
    """
    self._realAge = pRealAge;
    
  """
  """
  def getRealGender(self):
    """
    Returns the real gender of this person
    """
    return self._realGender;


  """
  """
  def setRealGender(self, pRealGender):
    """
    Sets the real gender of this person.
    """
    self._realGender = pRealGender;
    
  """
  """
  def getRelatedFaceHistoryIndex(self):
    """
    Returns the related face index of the face history pool from this person.
    """
    return self._relatedFaceHistoryIndex;


  """
  """
  def setRelatedFaceHistoryIndex(self, pRelatedFaceHistoryIndex):
    """
    Sets the related face index of the face history pool for this person.
    """
    self._relatedFaceHistoryIndex = pRelatedFaceHistoryIndex;
    
  """
  """
  def getSinceDate(self):
    """
    Returns the date at which this person was detected.
    """
    return self._sinceDate;

  """
  """
  def addToUnrecognizedCount(self):
    """
    updates the unrecognized count.
    """
    self._unrecognizedCount = self._unrecognizedCount + 1;

  """
  """
  def isUnrecognized(self):
    """ 
    Returns true whether this person is recognized or not.
    """
    return (self._unrecognizedCount > self.UNRECOGNIZED_THRESHOLD) and (self._relatedFaceHistoryIndex < 0);
        
    
  """
  """
  def setSinceDate(self, pSinceDate):
    """
    Sets the date at which this person was detected.
    """
    self._sinceDate = pSinceDate;
    
  def setIsHistoryFace(self, pIsHistoryFace):
    """
    Sets whether this face is a history face or not.
    """
    self._isAHistoryFace = pIsHistoryFace;
    
  def isAHistoryFace(self):
    """
    Returns if this face is a history face or not.
    """
    return self._isAHistoryFace;