#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Python OpenBR face template interface v0.1
by Iván de Paz Centeno
7/04/2015
'''

import brPyTemplate

class pyOBRFaceTemplate(brPyTemplate.pyOBRTemplate):
  """
  OpenBR Face template class. 
  Represent an OpenBR face template object. 
  
  The OpenBR template object is usually the result of an applied algorithm. Holds all the information
  of the operation, like the metadata, the file path and many other parameters.
  """
  
  """
  ****************************************************
  CONSTRUCTOR
  """
  def __init__(self, pPyOBRTemplate):
    """
    Initializes the template object.
    """
    self._openBRHandler = pPyOBRTemplate._openBRHandler;
    self._openBRTemplate = pPyOBRTemplate._openBRTemplate;
    self._destructionHandled = True;
  
  """
  ****************************************************
  isAConfidenceDetection() method
  """
  def isAConfidenceDetection(self):
    """
    Returns whether this detection ensures a face detection, otherwise false (a 
    probably false detection).
    """
    lValue = self.getMetadataValue("PossibleFTE");
    lResult = False;
    
    #print lValue;
    if lValue == "false":
      lResult = True;
      
    return lResult;
  
  """
  ****************************************************
  getFaceDetectionBounds() method
  """
  def getFaceDetectionBounds(self):
    """
    Returns the bounds of the recognized face.
    """
    lFrontalFaceData = self.getMetadataValue("FrontalFace").replace("(","").replace(")","");
    
    lResult = lFrontalFaceData.split(",");
    
    return lResult;
  
  """
  ****************************************************
  getEyeDetectionBounds() method
  """
  def getEyesPosition(self):
    """
    Returns the position of the eyes.
    """
    lLeftEye = self.getMetadataValue("Affine_0").replace("(","").replace(")","");
    lRightEye = self.getMetadataValue("Affine_1").replace("(","").replace(")","");
    
    lResult = lLeftEye.split(",") + lRightEye.split(",");
    
    return lResult;