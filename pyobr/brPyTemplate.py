#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Python OpenBR template interface v0.1
by Iván de Paz Centeno
26/03/2015
'''

import pyobr
import brPyHandler
from ctypes import *

class pyOBRTemplate(object):
  """
  OpenBR template class. 
  Represent an OpenBR template object. 
  
  The OpenBR template is biometric data represented using OpenCV matrices, with an associated File.
  
  Templates tends to have only one matrix, but it's not strange to have multiple matrices associated with a 
  given template in order to implement certain image processing transformations that either expect or reproduce
  multiple matrices.
  """
  
  _openBRHandler = None;
  _openBRTemplate = None;
  _destructionHandled = False;
  
  """
  ****************************************************
  CONSTRUCTOR
  """
  def __init__(self, pOpenBRHandler, pOpenBRTemplate, pDestructionHandled = False):
    """
    Initializes the template object.
    """
    self._openBRHandler = pOpenBRHandler;
    self._openBRTemplate = pOpenBRTemplate;
    self._destructionHandled = pDestructionHandled;
    
  
  """
  ****************************************************
  getData() method
  """
  def getData(self):
    """
    Returns the OpenBR data wrapped by this template.
    """
    return self._openBRTemplate;
  
  
  """
  ****************************************************
  _handlerOK() method
  """
  def _handlerOK(self):
    """
    Checks if the handler is OK to perform the OpenBR operations.
    
    Returns True if handler set and initialized. False if not.
    """
    
    return self._openBRHandler and self._openBRHandler.isInitialized();
  
  
  """
  """
  def getSize(self):
    """
    Returns the size of the template matrix in bytes.
    """
    return pyobr.extensionLib.templateSize (self._openBRTemplate);

  """
  """
  def getMatrixCount(self):
    """
    Returns the number of elements of the template cv matrix.
    """
    return pyobr.extensionLib.templateMatrixElementsCount (self._openBRTemplate);

  """
  ****************************************************
  getMetadataValue() method
  """
  def getMetadataValue(self, pMetadataKey):
    """
    Returns the desired value from the template metadata.
    This may be used to retrieve the age on an age estimation,
    the face position and many other parameters.
    
    If value isn't found, the return is None or empty string.
    """
    
    if not self._handlerOK() or not pMetadataKey or not self._openBRTemplate:
      return None;
    
    # C Api expects a buffer to write the value to. Let's create it:
    lRestBytes = 5;
    lBufferCount = 1;
    lStringBuffer = None;
    while (lRestBytes > lBufferCount):
      lBufferCount  = lRestBytes;
      lStringBuffer = create_string_buffer(lBufferCount); # this buffer is automatically freed by python on reference lost.
      lRestBytes =  pyobr.lib.br_get_metadata_string(lStringBuffer, lBufferCount, self._openBRTemplate, pMetadataKey);
      
      # Buffer size is autoadjusted to the metadata result length on the second iteration.
    
    lResult = None;
    
    if lStringBuffer:
      lResult = lStringBuffer.value;
    
    return lResult;
  
  
  def __del__(self):
    if not self._destructionHandled:
      pyobr.lib.br_free_template (self._openBRTemplate);