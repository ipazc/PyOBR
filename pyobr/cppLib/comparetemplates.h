#ifndef COMPARETEMPLATES_H
#define COMPARETEMPLATES_H

#include "comparetemplates_global.h"
#include <openbr/openbr_plugin.h>

/**
 * @brief The CompareTemplates class.
 *  Processes two templates in order to find its similarity value.
 *
 * @author Iván de Paz Centeno (ipazce00@estudiantes.unileon.es)
 * @date 03/06/2015
 */
class COMPARETEMPLATESSHARED_EXPORT CompareTemplates
{
    /**
     * @brief __distance a pointer to the br::Distance used to calculate the distance
     * between two templates.
     */
    QSharedPointer<br::Distance> __distance;
public:
    /**
     * @brief Constructor of the class. Initializes the BR (if not initialized yet).
     */
    CompareTemplates ();

    /**
     * @brief Destructor of the class. Finalizes the BR (if not finalized yet).
     */
    ~CompareTemplates ();

    /**
     * @brief Compares two templates to find the similarity value beyween both.
     * @param pTemplateA    template to compare.
     * @param pTemplateB    template to compare with.
     * @return float    value of similarity between both templates.
     */
    float Compare(const br::Template &pTemplateA, const br::Template &pTemplateB);
};

#endif // COMPARETEMPLATES_H
