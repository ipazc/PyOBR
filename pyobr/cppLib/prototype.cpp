#include "prototype.h"

float compareTemplates(void* pTemplateA, void* pTemplateB)
{
    CompareTemplates* lCompare = new CompareTemplates();
    br::Template* lTemplateA = (br::Template*) pTemplateA;
    br::Template* lTemplateB = (br::Template*) pTemplateB;

    return lCompare->Compare(*lTemplateA, *lTemplateB);
}


int templateMatrixSize(void* pTemplate)
{
    br::Template* lTemplate = (br::Template*) pTemplate;

    return lTemplate->m().step[0] * lTemplate->m().rows;
}

int templateMatrixElementsCount(void* pTemplate)
{
    br::Template* lTemplate = (br::Template*) pTemplate;

    return lTemplate->m().total();
}

int templateSize(void* pTemplate)
{
    br::Template* lTemplate = (br::Template*) pTemplate;

    return lTemplate->bytes();
}
