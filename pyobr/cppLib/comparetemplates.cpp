#include "comparetemplates.h"


CompareTemplates::CompareTemplates()
{
    //int argc = 1;
    //char app[] = "br";
    //char *argv[1] = {app};
    //br::Context::initialize(argc, argv, "");

    __distance = br::Distance::fromAlgorithm("FaceRecognition");
}

CompareTemplates::~CompareTemplates()
{
    //br::Context::finalize();
}

float CompareTemplates::Compare(const br::Template &pTemplateA, const br::Template &pTemplateB)
{
    return __distance->compare(pTemplateA, pTemplateB);
}
