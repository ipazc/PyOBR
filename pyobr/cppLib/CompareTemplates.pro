#-------------------------------------------------
#
# Project created by QtCreator 2015-06-01T17:01:44
#
#-------------------------------------------------

QT       -= gui

TARGET = CompareTemplates
TEMPLATE = lib

DEFINES += COMPARETEMPLATES_LIBRARY

SOURCES += comparetemplates.cpp \
    prototype.cpp

HEADERS += comparetemplates.h\
        comparetemplates_global.h \
    prototype.h

#QMAKE_CXXFLAGS += -std=c++0x
LIBS += -lopenbr

unix {
    target.path = /usr/lib
    INSTALLS += target
}
