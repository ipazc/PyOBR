#ifndef PROTOTYPE_H
#define PROTOTYPE_H

#include "comparetemplates.h"
#include <openbr/openbr_plugin.h>

/**
 * This is the openbr complemented API exported to python.
 */
extern "C"
{
    /**
     * @brief Compares two templates and returns its similarity value.
     * @param pTemplateA    template to compare.
     * @param pTemplateB    template to compare with.
     * @return  a value determining the similarity of both templates.
     */
    float compareTemplates(void* pTemplateA, void* pTemplateB);

    /**
     * @brief Returns the size of the template in bytes.
     * @param pTemplate     template to calculte size from.
     * @return  integer holding the size in bytes of the template.
     */
    int templateSize(void* pTemplate);

    /**
     * @brief Returns the size of the template matrix in bytes.
     * @param pTemplate     template to calculte size from.
     * @return  integer holding the size in bytes of the template matrix.
     */
    int templateMatrixSize(void* pTemplate);

    /**
     * @brief Returns the number of elements inside the template matrix.
     * @param pTemplate     template to calculte size from.
     * @return  integer holding the number of elements of the template matrix.
     */
    int templateMatrixElementsCount(void* pTemplate);
}
#endif // PROTOTYPE_H
