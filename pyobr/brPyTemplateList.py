#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Python OpenBR template list interface v0.1
by Iván de Paz Centeno
26/03/2015
'''

import pyobr
import brPyHandler
from brPyTemplate import pyOBRTemplate;
from ctypes import *

class pyOBRTemplateList(object):
  """
  OpenBR template list class. 
  Represent an OpenBR template list object. 
  
  The OpenBR template list object wraps a list of templates from the OpenBR lib.
  """
  
  __openBRHandler = None;
  __openBRTemplateList = None;
  
  """
  ****************************************************
  CONSTRUCTOR
  """
  def __init__(self, pOpenBRHandler, pOpenBRTemplateList):
    """
    Initializes the template object.
    """
    self.__openBRHandler = pOpenBRHandler;
    self.__openBRTemplateList = pOpenBRTemplateList;
  
  
  """
  ****************************************************
  getData() method
  """
  def getData(self):
    """
    Returns the OpenBR template list data wrapped by this template.
    """
    return self.__openBRTemplateList;
  
  
  
  """
  ****************************************************
  __handlerOK() method
  """
  def __handlerOK(self):
    """
    Checks if the handler is OK to perform the OpenBR operations.
    
    Returns True if handler set and initialized. False if not.
    """
    
    return self.__openBRHandler and self.__openBRHandler.isInitialized();
  
  
  """
  ****************************************************
  __get() method
  """
  def get(self, pIndex):
    """
    Returns the desired template from the list.
    
    If index out of bounds, None is return.
    """
    
    if not self.__handlerOK() or not self.__openBRTemplateList:
      return None;
    
    if (pIndex < 0) or (self.count() < pIndex + 1):
      return None;
    
    # We wrap the template inside the python template interface
    lTemplate = pyOBRTemplate (self.__openBRHandler, pyobr.lib.br_get_template( self.__openBRTemplateList, pIndex ), True);
    
    return lTemplate;
  
  
  """
  ****************************************************
  __getTemplate() method
  """
  def count(self):
    """
    Returns the number of elements in the list; -1 if something went wrong.
    """
    
    if not self.__handlerOK() or not self.__openBRTemplateList:
      return -1;

    return pyobr.lib.br_num_templates(self.__openBRTemplateList);
  
  
  def __del__(self):
    # List templates automatically handle each of the contained template release.
    pyobr.lib.br_free_template_list (self.__openBRTemplateList);