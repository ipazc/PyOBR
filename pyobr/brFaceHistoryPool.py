#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Python OpenBR Face history pool v0.1
by Iván de Paz Centeno
21/05/2015

Allows the search of faces in the history of detected faces 
Given a face, this class is capable of finding the closest one.
'''

import subprocess
import pyobr
import os.path
import brPyHandler
from brPyHandler import openBRHandler;
from brPyFace import pyOBRFace;
from brPyDB import pyOBRdb;
from brPyRecognitionTemplate import pyOBRRecognitionTemplate;

from ctypes import *
from PIL import Image

class pyOBRFaceHistoryPool(object):
  """
  OpenBR face history pool class. 
  Allows to perform checks against faces in order to get a face recognition.
  """
  __peopleList = [];
  __recognitionTemplateLists = [];
  
  __openBRHandler = None;
  
  
  __avatarsDir  = "/var/brpy/current/";
  __historyDir  = "/var/brpy/history/";
  
  DETECTION_THRESHOLD = 0.98;
  
  """
  ****************************************************
  CONSTRUCTOR
  """
  def __init__(self, pOpenBRHandler = openBRHandler):
    """
    Initializes the face history pool.
    """
    self.__openBRHandler = pOpenBRHandler;
    
  """
  ****************************************************
  __handlerOK() method
  """
  def __handlerOK(self):
    """
    Checks if the handler is OK to perform the OpenBR operations.
    
    Returns True if handler set and initialized. False if not.
    """
    
    return self.__openBRHandler and self.__openBRHandler.isInitialized();
  
  
  """
  """
  def __reloadList(self):
    """ 
    Reloads all the faces from the history people.
    """
    __recognitionTemplateLists = [];
    
    for lPerson in self.__peopleList:
        lFace = pyOBRFace(self.__openBRHandler);
        lFace.loadImg("{0}{1}.jpg".format(self.__historyDir, lPerson.getIndex()));
        lRecognitionTemplateList = lFace.recognition();
        lPerson.setAssociatedFace(lFace);
        lPerson.setRecognitionTemplateList(lRecognitionTemplateList);
        print "==================RELOADED;"
  
  
  def __comparePerson(self, pPerson, pWithPerson):
    """
    Compares the specified person with the other person.
    Returns a score value (float) between 0 and 1.
    """
    lPerson1Count = pPerson.getRecognitionTemplateList().count();
    lPerson2Count = pWithPerson.getRecognitionTemplateList().count();
    
    lResult = 0;
    
    if (lPerson1Count + lPerson2Count > 1):
        lPersonFace1 = pyOBRRecognitionTemplate(pPerson.getRecognitionTemplateList().get(0));
        lPersonFace2 = pyOBRRecognitionTemplate(pWithPerson.getRecognitionTemplateList().get(0));
        lResult = lPersonFace1.compareTo(lPersonFace2);
        
    # Now we compare both faces.
    return lResult;
   
    
    
  def indexOfRelatedHistoryPersonTo(self, pPerson):
    """
    Finds the related person to pPerson among the pool.
    
    Returns the index of the related person, or -1 if not found (depends on the DETECTION_THRESHOLD)
    """
    lResult = -1;
    
    # let's get the highest mark here:
    lScore = 0;
    lScorePersonIndex = 0;
    for lPerson in self.__peopleList:
        lNewScore = self.__comparePerson (pPerson, lPerson);
        
        if (lNewScore > lScore):
            lScore = lNewScore;
            lScorePersonIndex = lPerson.getIndex();
        
        if lScore == 1:
            break;
        
    # Must reach the threshold.
    if lScore < self.DETECTION_THRESHOLD:
        lScorePersonIndex = -1;
        
    return lScorePersonIndex;
    
    
  def getPeopleList(self):
    """
    Returns the list of people being processed by this pool class.
    """
    return self.__peopleList;



  def setPeopleList(self, pPeopleList):
    """
    Sets the people list to be processed by this pool class.
    """
    self.__peopleList = pPeopleList;
    self.__reloadList();
    
  """
  ****************************************************
  __DESTRUCTOR
  """
  def __del__(self):
    """
    Clean up the class data
    """
    # not need to unload the image. The unload is done to retrieve the actual 
    # image from the library, no to clean up its resources. That means 
    # the image can be unloaded as times as we want.
    