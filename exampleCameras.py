#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Ejemplo de uso de pyobr wrapper para manejar cámaras USB
'''

# Importamos la clase que gestiona la cámara
from pyobr.brPyCamera import pyOBRCamera;


# Conectamos con la cámara (en este caso, la cámara 0).
# Si existen más cámaras, se numeran de 0 a n.
lCamera = pyOBRCamera(0);   

lCamera.startRecording();

# Captura un frame y lo procesa en busca de caras. 
# Si le pasamos el parámetro "True" detectará todas las caras de la imagen.
if lCamera.processNextFrame(True):
    
    lPeopleList = lCamera.getPeopleList();
    
    print "Hay " + str(len(lPeopleList)) + " caras detectadas en la imagen.";
    
    # El frame obtenido se almacena en "/tmp/frame.jpg".
    
    for lPerson in lPeopleList:
        print "Persona detectada, coordenadas de la cara: " + str(lPerson.getFaceCoords());
        
        # mediante getAvatar() se puede extraer la imagen correspondiente a la 
        # cara en cuestion:
        
        #lAvatarImg = pPerson.getAvatar();
        #lAvatarImg.save("prueba.jpg", "JPEG");
        
        
    
    
lCamera.stopRecording();

print "\nFinalizado\n";